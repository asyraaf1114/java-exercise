/*
 * This project has no licence
 */
package simpleshiparraylist;

import java.util.ArrayList;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
class SimpleShip {
    private ArrayList<String> locations;
    
    void setLocationCells(ArrayList<String> locations) {
        this.locations = locations;
    }

    String checkYourself(String userGuess) {  
        String status = "miss";
        
        int index = locations.indexOf(userGuess);
        
        if (index >=0) {
            locations.remove(index);
            if (locations.isEmpty()) {
                status = "kill";
            } else {
                status = "hit";
            }
        }

        System.out.println(status);
             
        return status;
    }
}
