/*
 * This project has no licence
 */
package simpleshiparraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class SimpleShipArrayList {
    public static void main(String[] args) {
        SimpleShip ship = new SimpleShip();
        Scanner input = new Scanner (System.in);
        
        String str[] = {"2", "3", "4"};
              
        ArrayList<String> locations = new ArrayList<>();
        
        locations.addAll(Arrays.asList(str));

/*
 *      Second Method
 */
//        for (String s : str)
//            locations.add(s);
        
        ship.setLocationCells(locations);
        
        String result = null;
        int count = 0;
        
        while (result != "kill") {
            System.out.println("Enter an integer : ");
            String userGuess = input.next();
            result = ship.checkYourself(userGuess);
            count++;
        }
          
        System.out.println("Completed after " + count + " try/ies");
    }
}
