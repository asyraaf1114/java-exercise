/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package songtest;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class SongTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Song t1 = new Song();
        t1.setArtist("Aizat");
        t1.setTitle("Pergi");
        Song t2 = new Song();
        t2.setArtist("Faizal Tahir");
        t2.setTitle("Negaraku");
        
        t1.play();
        t2.play();
    }
    
}

class Song {
    private String title;
    private String artist;
    
    void setTitle(String title) {
        this.title = title;
    }
    
    void setArtist (String artist) {
        this.artist = artist;
    }
    
    void play () {
        System.out.println(this.artist + " " + this.title);
    }
}
