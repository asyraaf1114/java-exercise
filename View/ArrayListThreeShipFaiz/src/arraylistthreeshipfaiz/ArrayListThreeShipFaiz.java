package arraylistthreeshipfaiz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Faiz Iqbal
 */

public class ArrayListThreeShipFaiz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> locationsi = new ArrayList<>();
        ArrayList<String> locationsj = new ArrayList<>();
        ArrayList<String> locationsk = new ArrayList<>();
        ArrayList<SimpleShip> shipList = new ArrayList<>();
        String[] i = {"2", "3", "4"};
        String[] j = {"5", "6", "7"};
        String[] k = {"8", "9", "10"};

        locationsi.addAll(Arrays.asList(i));
        locationsj.addAll(Arrays.asList(j));
        locationsk.addAll(Arrays.asList(k));

        SimpleShip one = new SimpleShip();
        SimpleShip two = new SimpleShip();
        SimpleShip three = new SimpleShip();

        shipList.add(one);
        shipList.add(two);
        shipList.add(three);

        one.setLocationCells(locationsi);
        two.setLocationCells(locationsj);
        three.setLocationCells(locationsk);

        String result;
        int count = 0;
        int x = 1;
        boolean exitLoop = false;

        while (!shipList.isEmpty()) {
            if (!locationsi.isEmpty()) {
                System.out.println("Enter an integer :");
                String userGuess = input.next();
                result = one.checkYourself(userGuess);
                System.out.println(result);

                if (result.equals("kill")) {
                    System.out.println("You killed the " + x + "st ship");
                    shipList.remove(one);
                    x++;

                    exitLoop = true;
                }
                count++;
            } else if (!locationsj.isEmpty()) {
                System.out.println("Enter an integer :");
                String userGuess = input.next();
                result = two.checkYourself(userGuess);
                System.out.println(result);

                if (result.equals("kill")) {
                    System.out.println("You killed the " + x + "nd ship");
                    shipList.remove(two);
                    x++;
                }
                count++;
            } else {
                System.out.println("Enter an integer :");
                String userGuess = input.next();
                result = three.checkYourself(userGuess);
                System.out.println(result);

                if (result.equals("kill")) {
                    System.out.println("You killed the " + x + "rd ship");
                    shipList.remove(three);
                    x++;

                    exitLoop = true;
                }
                count++;
            }
        }

        System.out.println("You hit after " + count + " tries ");
    }
}
