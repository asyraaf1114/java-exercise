/*
 * This project has no licence
 */
package arraylistthreeshipfaiz;

import java.util.ArrayList;

/**
 *
 * @author Faiz Iqbal
 */
class SimpleShip {

    private ArrayList<String> locationCells;

    public void setLocationCells(ArrayList<String> locs) {
        locationCells = locs;
    }

    String checkYourself(String userInput) {
        String result = "Already Hit/Does Not Exist";

        int index = locationCells.indexOf(userInput);
        if (index >= 0) {
            locationCells.remove(index);

            if (locationCells.isEmpty()) {
                result = "kill";

            } else {
                result = "hit";

            }

        }
        return result;
    }
}
