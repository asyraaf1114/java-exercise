/*
 * No licence
 */
package simpleshiptestdrive;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
class SimpleShip {
    private int [] locations;
    private int numOfHits;
    
    void setLocationCells(int[] locations) {
        this.locations = locations;
    }

    String checkYourself(String userGuess) {
        
        int guess = Integer.parseInt (userGuess);
        
        String status = "miss";
        
        for (int i = 0; i < locations.length; i++) {
            if (locations[i] == guess) {
                status = "hit";              
                locations[i]=-guess;
                numOfHits++;
                break;
            } 
            else if (-guess==locations[i]){
                status="already hit";
            }
        }
        
        if (numOfHits == locations.length){
           
            status = "kill";
      
        }
        System.out.println(status);
             
        return status;
    }
}
