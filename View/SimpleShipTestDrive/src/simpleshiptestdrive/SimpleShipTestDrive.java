/*
 * No licence
 */
package simpleshiptestdrive;

import java.util.Scanner;


/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class SimpleShipTestDrive {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimpleShip ship = new SimpleShip();
        Scanner input = new Scanner (System.in);
        
        int[] locations = {2, 3, 4};
        ship.setLocationCells(locations);
        
        String result;
        boolean exitLoop=false;
        int count = 0;
        
        while (exitLoop==false) {
            System.out.println("Enter an integer : ");
            String userGuess = input.next();
            result = ship.checkYourself(userGuess);
            if (result=="kill"){
                exitLoop=true;
            }
            count++;
        }
          
        System.out.println("Completed after " + count + " try/ies");
    }
    
}