/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dogtestdrive;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class DogTestDrive {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Dog one = new Dog();
        one.size = 70;
        Dog two = new Dog();
        two.size = 6;
        Dog three = new Dog();
        three.size = 35;
        one.bark();
        two.bark();
        three.bark();
    }
    
}

class Dog {
    public int size;
    String name;
    
    void bark(){
        if (size > 60) {
            System.out.println("Wooof! Wooof!");
        } else if (size > 14) {
            System.out.println("Ruff!, Ruff!");
        } else {
            System.out.println("Yip! Yip!");
        }
    }
}
