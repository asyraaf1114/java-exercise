/*
 * This project has no licence
 */
package arraylistthreeshipasyraaf;

import java.util.ArrayList;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
class SimpleShip {
    private ArrayList<String> locations;
    private String name;
    
    void setName (String name){
        this.name = name;
    }
    
    String getName () {
        return this.name;
    }
    
    void setLocationCells(ArrayList<String> locations) {
        this.locations = new ArrayList<String>(locations);
    }
    
    ArrayList<String> getLocation() {
        return this.locations;
    }
    

    String checkYourself(String userGuess) {  
        String status = "miss/already hit";
        
        int index = locations.indexOf(userGuess);
        
        if (index >=0) {
            locations.remove(index);
            if (locations.isEmpty()) {
                status = "kill";
            } else {
                status = "hit";
            }
        }

        System.out.println(status);
             
        return status;
    }
}
