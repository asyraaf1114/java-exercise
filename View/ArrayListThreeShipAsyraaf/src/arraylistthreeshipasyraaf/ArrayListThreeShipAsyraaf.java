/*
 * This project has no licence
 */
package arraylistthreeshipasyraaf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class ArrayListThreeShipAsyraaf {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        final int NUM_SHIP = 3;
        final String[] name = new String[]{"one", "two", "three"};
        
        ArrayList <SimpleShip> shipList = new ArrayList<SimpleShip>();

        ArrayList<String> locations = new ArrayList<>();
        locations.addAll(Arrays.asList("2","3","4"));
        
        Scanner input = new Scanner (System.in);
        
        for (int i = 0; i < NUM_SHIP; i++) {
            shipList.add(new SimpleShip());
            shipList.get(i).setName(name[i]);
            shipList.get(i).setLocationCells(locations);
        }
        
        int numGuess = 0;
        
        while (!shipList.isEmpty()) {
            Iterator iterator = shipList.iterator();
            while (iterator.hasNext()){
                SimpleShip s = (SimpleShip) iterator.next();
                System.out.print("Enter guess for ship " + s.getName() + " : ");
                String userGuess = input.next();
                String status = s.checkYourself(userGuess);
                if (status.equals("kill")) {
                    System.out.println("Ship " + s.getName() + " killed");
                    iterator.remove();
                }
                numGuess++;
            }
        }
        
        System.out.println("Total number of guess is " + numGuess);
    }
    
}
