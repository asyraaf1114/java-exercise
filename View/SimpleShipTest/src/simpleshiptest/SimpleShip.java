/*
 * No licence
 */
package simpleshiptest;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
class SimpleShip {
    private int [] locations;
    private boolean[] state;
    private int numOfHits;
    
    void setLocationCells(int[] locations) {
        this.locations = locations;
        this.state = new boolean[locations.length];
    }

    String checkYourself(String userGuess) {
        
        int guess = Integer.parseInt (userGuess);
        
        String status = "miss";
        
        for (int i = 0; i < locations.length; i++) {
            if (locations[i] == guess) {
                if (!state[i]) {
                    status = "hit";              
                    state[i]= true;
                    numOfHits++;
                } else {
                    status = "already hit";
                }
                break;
            } 
        }
        
        if (numOfHits == locations.length){     
            status = "kill";   
        }
        System.out.println(status);
             
        return status;
    }
}
