/*
 * No licence
 */
package simpleshiptest;
import java.util.Scanner;

/**
 *
 * @author Asyraaf <asyraaf21221@gmail.com>
 */
public class SimpleShipTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimpleShip ship = new SimpleShip();
        Scanner input = new Scanner (System.in);
        
        int[] locations = {2, 3, 4};
        ship.setLocationCells(locations);
        
        String result = null;
        int count = 0;
        
        while (result != "kill") {
            System.out.println("Enter an integer : ");
            String userGuess = input.next();
            result = ship.checkYourself(userGuess);
            count++;
        }
          
        System.out.println("Completed after " + count + " try/ies");
    }
    
}